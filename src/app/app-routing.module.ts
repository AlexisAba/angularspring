import { NgModule } from '@angular/core';
import { AppComponent } from 'src/app/app.component';
import { Routes, RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { routing } from './app-routing';
import { LoginComponent } from './login/login.component';


const routes: Routes = [];

@NgModule({
    imports: [
        BrowserModule,
        routing
    ],
    declarations: [
        AppComponent,
        LoginComponent
    ],
    bootstrap: [AppComponent]
})

export class AppModule { }
